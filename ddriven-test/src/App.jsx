import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import FlightList from './views/flightList';

// mocked data for flight list 
const flightList = [{
  from: 'DEL',
  to: 'ALD',
  departure: '24/04/2018',
  arrival: '24/04/2018',
  price: '5000'
},
{
  from: 'DEL',
  to: 'ALD',
  departure: '24/04/2018',
  arrival: '25/04/2018',
  price: '5000'
},
{
  from: 'PJB',
  to: 'DEL',
  departure: '24/04/2018',
  arrival: '24/04/2018',
  price: '2000'
},
{
  from: 'LKO',
  to: 'ALD',
  departure: '24/04/2018',
  arrival: '24/04/2018',
  price: '4000'
},
{
  from: 'ALD',
  to: 'DEL',
  departure: '25/04/2018',
  arrival: '25/04/2018',
  price: '5000'
}];

class App extends Component {

  constructor() {
    
    super();
    this.state = {
      sForm: '',
      sTo: '',
      sDeparture: '',
      sArrival: '',
      flightList: flightList
    };
  } 

  searchFlights = (event) => {
    this.resetFlightList();
    this.applySearch();
    event.preventDefault();
  };
  
  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  resetFlightList = () => {
    this.setState({flightList: flightList});
  };

  applySearch = () => {
    this.searchResult = this.state.flightList.filter((flight) => {
      return flight.from === this.state.sForm && 
              flight.to === this.state.sTo && 
              flight.departure >= this.state.sDeparture && 
              flight.arrival <= this.state.sArrival;
    });
    this.setState({flightList: this.searchResult});
  };


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Flight search engine</h1>
        </header>

        {/* search form */}
        <div className="search-form">
          Enter origin city: <input type="text" name="sForm" value= {this.state.sForm} onChange={this.handleInputChange} /> <br/>
          Enter destination city: <input type="text" name="sTo" value= {this.state.sTo} onChange={this.handleInputChange} /> <br/>
          Enter departure date :  <input type="text" name="sDeparture" value= {this.state.sDeparture} onChange={this.handleInputChange} /> <br/>
          Enter arrival date :  <input type="text" name="sArrival" value= {this.state.sArrival} onChange={this.handleInputChange} /> <br/>
          <button onClick={this.searchFlights}>Search</button>
        </div>

        {/* flight list- separate component */}
        {/* <div className="flight-list">
          {this.state.flightList.map((flight) => {
            return (
              <div class="flight">
                <span className="from spacing">From- {flight.from}</span>
                <span className="to spacing">To- {flight.to}</span>
                <br/>
                <span className="Price spacing">Rs {flight.price}</span>
                <br/>
                <span className="departure spacing">Departure Date- {flight.departure}</span>
                <br/>
                <span className="departure spacing">Arrival Date- {flight.arrival}</span>
              </div>  
            );
          })}
          
        </div> */}

        {/* using component */}
        <FlightList {...this.state} />
      </div>
    );
  }
}

export default App;
