import React, { Component } from 'react';
import './flightList.css';

class FlightList extends Component {
  render() {
    return (
        <div className="flight-list">
          {this.props.flightList.map((flight) => {
            return (
              <div className="flight">
                <span className="from spacing">From- {flight.from}</span>
                <span className="to spacing">To- {flight.to}</span>
                <br/>
                <span className="Price spacing">Rs {flight.price}</span>
                <br/>
                <span className="departure spacing">Departure Date- {flight.departure}</span>
                <br/>
                <span className="departure spacing">Arrival Date- {flight.arrival}</span>
              </div>  
            );
          })}
        </div>        
    );
  }
}

export default FlightList;
